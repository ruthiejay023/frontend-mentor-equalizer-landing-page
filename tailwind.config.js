/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      'light-yellow': '#FFB964',
      'light-blue': '#66E2DC',
      'light-orange': '#FA7453',
      'off-white': '#FCFAF9',
      'off-black': '#191826',
      'yellow': '#FF9201',
      'sky-blue':'#40FFF6'
    },
    fontFamily: {
      IbmPlexSans: ['IBM Plex Sans', 'cursive']
    }
  },
  plugins: [],
}

